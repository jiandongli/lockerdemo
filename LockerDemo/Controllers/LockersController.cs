﻿using LockerDemo.Models.Entities;
using LockerDemo.Models.EntityManager;
using LockerDemo.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LockerDemo.Controllers
{
    public class LockersController : Controller
    {
        LockersManager lockersManager = new LockersManager();

        /// <summary>
        /// Get Locker Bank from all the location
        /// </summary>
        /// <returns>A view contain a list of location model</returns>
        public ActionResult Index()
        {
            try
            {
                List<LBLocationEntity> lbLocations = lockersManager.GetLBLocations();
                List<LockerBankEntity> lockerBanks = lockersManager.GetLockerBanks();

                List<LBLocationView> LBLocationView = new List<LBLocationView>();

                foreach (var location in lbLocations)
                {
                    LBLocationView locationView = new LBLocationView();
                    List<LockerBankEntity> lockerBanksRegion = lockerBanks.FindAll(o => o.LBLocationID == location.LBLocationID).ToList();
                    List<LockerBankEntity> lockerBankList = new List<LockerBankEntity>();

                    foreach (var lockerBankRegion in lockerBanksRegion)
                    {
                        LockerBankEntity lockerBank = new LockerBankEntity();
                        lockerBank.LBLocationID = lockerBankRegion.LBLocationID;
                        lockerBank.LockerBankerID = lockerBankRegion.LockerBankerID;
                        lockerBank.LBName = lockerBankRegion.LBName;

                        lockerBankList.Add(lockerBank);
                    }

                    //View model binding
                    locationView.Country = location.Country;
                    locationView.Region = location.Region;
                    locationView.LBLocationID = location.LBLocationID;
                    locationView.LockerBank = lockerBankList.ToList();
                    LBLocationView.Add(locationView);
                }

                return View(LBLocationView);
            }
            catch(Exception ex)
            {
                TempData["msg"] = "<script>alert('System error, please try again');</script>";
                return View();
            }
            
        }

        /// <summary>
        /// Get Lockers from a specific Locker Bank
        /// </summary>
        /// <param name="id">locker bank ID</param>
        /// <returns>A view contain a locker bank model</returns>
        public ActionResult LockerBank(string id)
        {
            try
            {
                if (!string.IsNullOrEmpty(id.Trim()))
                {
                    LockerBankEntity lockerBankEntity = lockersManager.GetLockerBankById(id);
                    LockerBankView lockerBankView = new LockerBankView();
                    List<LockerEntity> lockers = lockersManager.GetLockersByLockerBank(id);

                    //View model binding
                    lockerBankView.LBLocationID = lockerBankEntity.LBLocationID;
                    lockerBankView.LockerBankerID = lockerBankEntity.LockerBankerID;
                    lockerBankView.LBName = lockerBankEntity.LBName;
                    lockerBankView.Locker = lockers;

                    return View(lockerBankView);
                }
                else
                {
                    return RedirectToAction("index");
                }
            }
            catch (Exception ex)
            {
                TempData["msg"] = "<script>alert('Fail to load the locker bank, please try again');</script>";
                return RedirectToAction("Index");
            }
        }
    }
}