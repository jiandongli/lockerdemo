﻿using LockerDemo.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LockerDemo.Models.EntityManager
{
    interface LockersManagerInterface
    {
        List<LBLocationEntity> GetLBLocations();
        List<LockerBankEntity> GetLockerBanks();
        LockerBankEntity GetLockerBankById(string lockerBankID);
        List<LockerEntity> GetLockersByLockerBank(string lockerBankID);
    }
}
