﻿using LockerDemo.Models.DB;
using LockerDemo.Models.Entities;
using LockerDemo.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LockerDemo.Models.EntityManager
{
    public class LockersManager : LockersManagerInterface
    {
        /// <summary>
        /// Get all the locations
        /// </summary>
        /// <returns>a list contain of all the locations</returns>
        public List<LBLocationEntity> GetLBLocations()
        {
            List<LBLocationEntity> lBLocationList = new List<LBLocationEntity>();

            using (LockDemoDbEntities db = new LockDemoDbEntities())
            {
                var locations = db.LBLocation.ToList();
                foreach (var location in locations)
                {
                    LBLocationEntity LBLocationEntity = new LBLocationEntity();
                    LBLocationEntity.LBLocationID = location.LBLocationID.ToString();
                    LBLocationEntity.Country = location.Country;
                    LBLocationEntity.Region = location.Region;
                    lBLocationList.Add(LBLocationEntity);
                }
            }

            return lBLocationList;
        }

        /// <summary>
        /// Get all the locker banks
        /// </summary>
        /// <returns>a list contain of all the locker bank</returns>
        public List<LockerBankEntity> GetLockerBanks()
        {
            List<LockerBankEntity> LockerBankList = new List<LockerBankEntity>();

            using (LockDemoDbEntities db = new LockDemoDbEntities())
            {
                var lockerBanks = db.LockerBank;
                foreach (var lockerBank in lockerBanks)
                {
                    LockerBankEntity LockerBankEntity = new LockerBankEntity();
                    LockerBankEntity.LBLocationID = lockerBank.LBLocationID.ToString();
                    LockerBankEntity.LBName = lockerBank.LBName;
                    LockerBankEntity.LockerBankerID = lockerBank.LockerBankID.ToString();
                    LockerBankList.Add(LockerBankEntity);
                }
            }

            return LockerBankList;
        }

        /// <summary>
        /// Get a specific locker bank based on a locker bank ID
        /// </summary>
        /// <param name="lockerBankID"></param>
        /// <returns>A locker bank entity</returns>
        public LockerBankEntity GetLockerBankById(string lockerBankID)
        {
            LockerBankEntity lockerBankEntity = new LockerBankEntity();

            using (LockDemoDbEntities db = new LockDemoDbEntities())
            {
                var lockerBank = db.LockerBank.Where(o => o.LockerBankID == new Guid(lockerBankID)).First();

                lockerBankEntity.LBLocationID = lockerBank.LBLocationID.ToString();
                lockerBankEntity.LockerBankerID = lockerBank.LockerBankID.ToString();
                lockerBankEntity.LBName = lockerBank.LBName;
            }

            return lockerBankEntity;
        }

        /// <summary>
        /// Get all the lockers from a specific locker bank
        /// </summary>
        /// <param name="lockerBankID"></param>
        /// <returns>A locker bank entity contains all lockers' detail</returns>
        public List<LockerEntity> GetLockersByLockerBank(string lockerBankID)
        {
            List<LockerEntity> lockerList = new List<LockerEntity>();

            using (LockDemoDbEntities db = new LockDemoDbEntities())
            {
                var lockers = db.Locker.Where(o => o.LockerBankID == new Guid(lockerBankID));
                foreach (var locker in lockers)
                {
                    LockerEntity LockerEntity = new LockerEntity();
                    LockerEntity.LockerBankID = locker.LockerBankID.ToString();
                    LockerEntity.LockerName = locker.LockerName;
                    LockerEntity.LockerID = locker.LockerID.ToString();
                    lockerList.Add(LockerEntity);
                }
            }

            return lockerList;
        }
    }
}