﻿using LockerDemo.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LockerDemo.Models.ViewModel
{
    public class LockerBankView
    {
        public string LockerBankerID { get; set; }
        public string LBName { get; set; }
        public string LBLocationID { get; set; }

        public ICollection<LockerEntity> Locker { get; set; }
    }
}