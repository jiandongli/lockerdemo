﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LockerDemo.Models.Entities
{
    public class LockerEntity
    {
        public string LockerID { get; set; }
        public string LockerName { get; set; }
        public string LockerBankID { get; set; }
    }
}