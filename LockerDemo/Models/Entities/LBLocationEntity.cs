﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LockerDemo.Models.Entities
{
    public class LBLocationEntity
    {
        public string LBLocationID { get; set; }
        public string Country { get; set; }
        public string Region { get; set; }

        public virtual ICollection<LockerBankEntity> LockerBank { get; set; }
    }
}